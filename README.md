# Flutter Test Project
 
# For Clone Project : 
git clone https://gitlab.com/ravip.work/flutter-app-test-demo.git

# run project 
For web : select project as chrome in and shift + F10
For mobile : select project as  mobile in and shift + F10

# Using plugin in the Project 

# 1. responsive_framework 
Used for responsive design
 
# 2. google fonts 
Used For google font

# 3. flutter_svg  
Used for load svg image in the project

# 4. material_segmented_control
Used for sagment button (tab bar ) 
