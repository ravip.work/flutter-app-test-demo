import 'package:flutter/material.dart';
import 'package:flutter_responsive_framework_tutorial/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'common/constants_colors.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 80,
        decoration: const BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),),
        child: BottomAppBar(
          child: Container(
            height: 80,
            decoration: const BoxDecoration(
                color: white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                )),
            child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 30.0, vertical: 10),
                  child: Container(
                      height: 25,
                      decoration: const BoxDecoration(
                          color: buttonColor,
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Center(
                          child: Text(
                            'Kostenlos Registrieren',
                            style: GoogleFonts.lato(
                                fontWeight: FontWeight.normal,
                                fontSize: 10,
                                color: blue),
                          ))),
                )),
          ),
          elevation: 0,
        ),
      ),
      appBar: AppBar(
        toolbarHeight: 67,
        shape: const ContinuousRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        backgroundColor: white,
        actions: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            child: Row(
              children: [
                Text(
                  "Jetzt Klicken",
                  style: GoogleFonts.lato(
                    textStyle: const TextStyle(
                      fontFamily: 'normal',
                      color: black,
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                OutlinedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0))),
                  ),
                  onPressed: () {},
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: const Text(
                      "Kostenlos Registrieren",
                      style: TextStyle(color: menuColor, fontSize: 14),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  "Login",
                  style: GoogleFonts.lato(
                    textStyle: const TextStyle(
                      fontFamily: 'normal',
                      color: menuColor,
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          Container(
            color: appBackColor,
            child: ResponsiveRowColumn(
              rowMainAxisAlignment: MainAxisAlignment.spaceEvenly,
              rowPadding: const EdgeInsets.all(30),
              columnPadding: const EdgeInsets.all(30),
              layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                  ? ResponsiveRowColumnType.COLUMN
                  : ResponsiveRowColumnType.ROW,
              children: [
                ResponsiveRowColumnItem(rowFlex: 1, child: TitleTile()),
                ResponsiveRowColumnItem(rowFlex: 1, child: TitleTileImage()),
              ],
            ),
          ),
          const SizedBox(
            height: 60,
          ),
          Container(
            child: CenterContent(),
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
