import 'package:flutter/material.dart';

const white = Color(0xffFFFFFF);
const black = Color(0xff2F2F2F);
const appBackColor = Color(0xffe8fafc);
const menuColor = Color(0xff319795);
const Color primaryColor = Colors.black;
const Color blue = Color(0xFFE6FFFA);
const Color buttonColor = Color(0xFF319795);
const Color textColor = Color(0xFF4A5568);
const Color tabColor = Color(0xFF81E6D9);
