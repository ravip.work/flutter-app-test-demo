import 'package:flutter/material.dart';
import 'package:flutter_responsive_framework_tutorial/common/constants_colors.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:responsive_framework/responsive_framework.dart';


class AppBarTitle extends StatelessWidget {
  const AppBarTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      "Login",
      style: GoogleFonts.lato(
        textStyle: const TextStyle(
          fontFamily: 'normal',
          color: menuColor,
          fontWeight: FontWeight.w400,
          fontSize: 16,
        ),
      ),
    );
  }
}

class TitleTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(
            'Deine job',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: ResponsiveValue(
                context,
                defaultValue: 60.0,
                valueWhen: const [
                  Condition.smallerThan(
                    name: MOBILE,
                    value: 40.0,
                  ),
                  Condition.largerThan(
                    name: TABLET,
                    value: 80.0,
                  )
                ],
              ).value,
              color: Colors.blueGrey[900],
              fontWeight: FontWeight.w700,
            ),
          ),
          Text(
            'website',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: ResponsiveValue(
                context,
                defaultValue: 60.0,
                valueWhen: const [
                  Condition.smallerThan(
                    name: MOBILE,
                    value: 40.0,
                  ),
                  Condition.largerThan(
                    name: TABLET,
                    value: 80.0,
                  )
                ],
              ).value,
              color: Colors.blueGrey[900],
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 65),
          ResponsiveVisibility(
            visible: false,
            visibleWhen: [Condition.largerThan(name: TABLET)],
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Container(
                  width: 320,
                  height: 40,
                  decoration: const BoxDecoration(
                      color: buttonColor,
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: Center(
                      child: Text(
                    'Kostenlos Registrieren',
                    style: GoogleFonts.lato(
                        fontWeight: FontWeight.normal,
                        fontSize: 10,
                        color: blue),
                  ))),
            ),
          ),
        ],
      ),
    );
  }
}

class TitleTileImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          ResponsiveVisibility(
            visible: false,
            visibleWhen: const [
              Condition.largerThan(name: TABLET),
              Condition.largerThan(name: MOBILE)
            ],
            child: Container(
              height: 400,
              width: 400,
              decoration: const BoxDecoration(
                  color: Colors.white, shape: BoxShape.circle),
              child: ClipOval(
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(120), // Image radius
                  child: SvgPicture.asset('assets/agreement.svg',
                      fit: BoxFit.fitWidth),
                ),
              ),
            ),
          ),
          ResponsiveVisibility(
              visible: false,
              visibleWhen: const [Condition.equals(name: MOBILE)],
              child: SvgPicture.asset('assets/agreement.svg',
                  fit: BoxFit.fitWidth)),
        ],
      ),
    );
  }
}

class CenterContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int _currentSelection = 0;
    Map<int, Widget> _children = {
      0: const Text('Arbeitnehmer'),
      1: const Text('Arbeitgeber'),
      2: const Text('Temporärbüro'),
    };
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          SizedBox(
            width: 400,
            child: MaterialSegmentedControl(
              verticalOffset: 8.0,
              children: _children,
              selectionIndex: _currentSelection,
              borderColor: Colors.grey,
              selectedColor: tabColor,
              unselectedColor: Colors.white,
              selectedTextStyle: GoogleFonts.lato(
                  fontWeight: FontWeight.normal, fontSize: 12, color: white),
              unselectedTextStyle: GoogleFonts.lato(
                  fontWeight: FontWeight.normal,
                  fontSize: 12,
                  color: buttonColor),
              borderWidth: 0.7,
              borderRadius: 12.0,
              onSegmentTapped: (index) {},
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          Text(
            'Drei einfache Schritte\n zu deinem neuen Job',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: ResponsiveValue(
                context,
                defaultValue: 25.0,
                valueWhen: const [
                  Condition.smallerThan(
                    name: MOBILE,
                    value: 10.0,
                  ),
                  Condition.largerThan(
                    name: TABLET,
                    value: 40.0,
                  )
                ],
              ).value,
              color: textColor,
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(
            height: 80,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                '1.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ResponsiveValue(
                    context,
                    defaultValue: 25.0,
                    valueWhen: const [
                      Condition.smallerThan(
                        name: MOBILE,
                        value: 10.0,
                      ),
                      Condition.largerThan(
                        name: TABLET,
                        value: 130.0,
                      )
                    ],
                  ).value,
                  color: textColor,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 5),
                child: Text(
                  'Erstellen dein Lebenslauf.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ResponsiveValue(
                      context,
                      defaultValue: 15.0,
                      valueWhen: const [
                        Condition.smallerThan(
                          name: MOBILE,
                          value: 8.0,
                        ),
                        Condition.largerThan(
                          name: TABLET,
                          value: 30.0,
                        )
                      ],
                    ).value,
                    color: textColor,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              const SizedBox(
                width: 60,
              ),
              Container(
                height: ResponsiveValue(
                  context,
                  defaultValue: 150.0,
                  valueWhen: const [
                    Condition.equals(
                      name: MOBILE,
                      value: 140.0,
                    ),
                    Condition.smallerThan(
                      name: MOBILE,
                      value: 50.0,
                    ),
                    Condition.largerThan(
                      name: TABLET,
                      value: 400.0,
                    )
                  ],
                ).value,
                width: ResponsiveValue(
                  context,
                  defaultValue: 150.0,
                  valueWhen: const [
                    Condition.equals(
                      name: MOBILE,
                      value: 100.0,
                    ),
                    Condition.smallerThan(
                      name: MOBILE,
                      value: 50.0,
                    ),
                    Condition.largerThan(
                      name: TABLET,
                      value: 400.0,
                    )
                  ],
                ).value,
                child: SvgPicture.asset('assets/list_image_first.svg',
                    fit: BoxFit.fitWidth),
              ),
            ],
          ),
          const SizedBox(
            height: 80,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                height: ResponsiveValue(
                  context,
                  defaultValue: 150.0,
                  valueWhen: const [
                    Condition.equals(
                      name: MOBILE,
                      value: 140.0,
                    ),
                    Condition.smallerThan(
                      name: MOBILE,
                      value: 50.0,
                    ),
                    Condition.largerThan(
                      name: TABLET,
                      value: 400.0,
                    )
                  ],
                ).value,
                width: ResponsiveValue(
                  context,
                  defaultValue: 150.0,
                  valueWhen: const [
                    Condition.equals(
                      name: MOBILE,
                      value: 100.0,
                    ),
                    Condition.smallerThan(
                      name: MOBILE,
                      value: 50.0,
                    ),
                    Condition.largerThan(
                      name: TABLET,
                      value: 400.0,
                    )
                  ],
                ).value,
                child: SvgPicture.asset('assets/list_image_second.svg',
                    fit: BoxFit.fitWidth),
              ),
              const SizedBox(
                width: 60,
              ),
              Text(
                '2.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ResponsiveValue(
                    context,
                    defaultValue: 25.0,
                    valueWhen: const [
                      Condition.smallerThan(
                        name: MOBILE,
                        value: 10.0,
                      ),
                      Condition.largerThan(
                        name: TABLET,
                        value: 130.0,
                      )
                    ],
                  ).value,
                  color: textColor,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 5),
                child: Text(
                  'Erstellen dein Lebenslauf.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ResponsiveValue(
                      context,
                      defaultValue: 15.0,
                      valueWhen: const [
                        Condition.smallerThan(
                          name: MOBILE,
                          value: 8.0,
                        ),
                        Condition.largerThan(
                          name: TABLET,
                          value: 30.0,
                        )
                      ],
                    ).value,
                    color: textColor,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 100,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                '3.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ResponsiveValue(
                    context,
                    defaultValue: 25.0,
                    valueWhen: const [
                      Condition.smallerThan(
                        name: MOBILE,
                        value: 10.0,
                      ),
                      Condition.largerThan(
                        name: TABLET,
                        value: 130.0,
                      )
                    ],
                  ).value,
                  color: textColor,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 5),
                child: Text(
                  'Mit nur einem Klick bewerben.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: ResponsiveValue(
                      context,
                      defaultValue: 15.0,
                      valueWhen: const [
                        Condition.smallerThan(
                          name: MOBILE,
                          value: 8.0,
                        ),
                        Condition.largerThan(
                          name: TABLET,
                          value: 30.0,
                        )
                      ],
                    ).value,
                    color: textColor,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              const SizedBox(
                width: 40,
              ),
              Container(
                height: ResponsiveValue(
                  context,
                  defaultValue: 150.0,
                  valueWhen: const [
                    Condition.equals(
                      name: MOBILE,
                      value: 140.0,
                    ),
                    Condition.smallerThan(
                      name: MOBILE,
                      value: 50.0,
                    ),
                    Condition.largerThan(
                      name: TABLET,
                      value: 400.0,
                    )
                  ],
                ).value,
                width: ResponsiveValue(
                  context,
                  defaultValue: 150.0,
                  valueWhen: const [
                    Condition.equals(
                      name: MOBILE,
                      value: 100.0,
                    ),
                    Condition.smallerThan(
                      name: MOBILE,
                      value: 50.0,
                    ),
                    Condition.largerThan(
                      name: TABLET,
                      value: 400.0,
                    )
                  ],
                ).value,
                child: SvgPicture.asset('assets/list_image_third.svg',
                    fit: BoxFit.fitWidth),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class RegistrationBlock extends StatelessWidget {
  const RegistrationBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ResponsiveVisibility(
          visible: false,
          visibleWhen: const [Condition.equals(name: MOBILE)],
          child: Container(
            width: 240,
            height: 25,
            decoration: BoxDecoration(
              border: Border.all(
                color: white,
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Container(
                width: 240,
                height: 25,
                decoration: const BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                child: Center(
                  child: Text(
                    'Kostenlos Registrieren',
                    style: GoogleFonts.lato(
                        fontWeight: FontWeight.normal,
                        fontSize: 10,
                        color: blue),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
